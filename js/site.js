$(document).ready(function () {

    $(window).scroll(function (event) {

        $('.fade-in').each(function () {
            var top_of_element = $(this).offset().top;
            var bottom_of_element = $(this).offset().top + $(this).outerHeight();
            var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
            var top_of_screen = $(window).scrollTop();

            if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) && !$(this).hasClass('is-visible')) {
                $(this).addClass('is-visible');
            }
        });
    });

    $("#SurveyTrigger").click(function (e) {
        e.preventDefault();
        (function (t, e, s, o) { var n, c, l; t.SMCX = t.SMCX || [], e.getElementById(o) || (n = e.getElementsByTagName(s), c = n[n.length - 1], l = e.createElement(s), l.type = "text/javascript", l.async = !0, l.id = o, l.src = "https://widget.surveymonkey.com/collect/website/js/tRaiETqnLgj758hTBazgd9E5LGzbxL5zIm3dX3g0ymSBctoMef2odAYB0JfWtgCr.js", c.parentNode.insertBefore(l, c)) })(window, document, "script", "smcx-sdk");
    });
});